import hashlib
import json
import requests
from flask import Flask, request, jsonify, Response
    
def getSHA256 (hash_msg):

    hash_object = hashlib.sha256(hash_msg)
    hex_dig = hash_object.hexdigest()
    return (hex_dig)

