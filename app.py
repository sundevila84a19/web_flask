import os
from flask import Flask, request, jsonify, Response
from lib import *

app = Flask(__name__)
app.config.from_object(os.environ['APP_SETTINGS'])
# print(os.environ['APP_SETTINGS'])

sha_dict_msg = {}
sha_dict_dig = {}

@app.route('/')
def hello():
    test_print()
    return "Hello World!"
    

@app.route('/messages',methods = ['POST'])
def api_messages():
        
    # ---!!
    print request.data
    # ---!!
    
    # convert jason to dictionary
    api_input = json.loads(request.data)
    
    api_output = {}
    
    if (request.headers['Content-Type'] == 'application/json'):
        if (sha_dict_dig.has_key(api_input['message'])):
            api_output['message'] =  sha_dict_dig[api_input['message']]
        else:
            hex_dig = getSHA256 (api_input['message'])    
            sha_dict_msg[api_input['message']] = hex_dig
            sha_dict_dig[hex_dig] = api_input['message'] 
            api_output = {'message':hex_dig}
    
    # convert dictionary to jason
    js = json.dumps(api_output)
    return js

@app.route('/messages/<input_dig>',methods = ['POST','GET'])
def api_dig(input_dig):

    print "input_dig:" + input_dig
    
    api_output = {}
    
    if (sha_dict_dig.has_key(input_dig)):
        api_output['message'] =  sha_dict_dig[input_dig]
    else:
        return not_found()
    
    # convert dictionary to jason
    js = json.dumps(api_output)
    return js

@app.errorhandler(404)
def not_found(error=None):
    message = {
        'err_msg': "Message not found",    
    }
    js = jsonify(message)
    js.status_code = 404

    return js


if __name__ == '__main__':
    app.run()

